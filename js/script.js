window.inject = function (e) {
    if(e.value===undefined || e.value==='') return;
    tinymce.execCommand('mceInsertContent', false, '[ms_webform id=\'' + e.value + '\']');
    jQuery("#webforms").val('');
};
(function () {
    jQuery.post(ajaxurl, {action:'load_webforms'}, function (response) {
        window.team_id = response.team_id;
        JSON.parse(response).webforms.forEach(function (webform) {
            var webformId = webform.webform_id;
            var webformTitle = JSON.parse(webform.title).text;
            jQuery("#webforms").append(jQuery("<option value='"+webformId+"'>"+webformTitle+"</option>"));
        });
        jQuery("#webforms").val('');
    });
})();