<?php

/*
Plugin Name: MailScout Webform Integration
Plugin URI: http://mailscout.io/
Description: Integrate MailScout Webform with WordPress
Version: 1.0
Author: Rocky
Author URI: http://rocky.pw/
License: GPL3
*/


if(!class_exists('MS_WebForm')) {
	class MS_WebForm {
		private static $instance;
		private function __construct() {
			register_activation_hook(__FILE__, [&$this, 'activated']);
			register_deactivation_hook(__FILE__, [&$this, 'deactivated']);
            if(is_admin()) {
	            add_action( 'admin_init', [ &$this, 'admin_init' ] );
	            add_action( 'admin_menu', [ &$this, 'admin_menu' ] );

	            add_action( 'media_buttons', [ &$this, 'add_media_button' ] );
	            add_action( 'wp_ajax_load_webforms', [ &$this, 'load_webforms' ] );
            }
			add_shortcode('ms_webform', [&$this, 'get_web_form']);
		}

		public function activated() {
			//do something
		}
		public function deactivated() {
			delete_option("ms_api_url");
			delete_option("ms_api_token");
		}

		public static function start() {
			if(self::$instance!=null) return self::$instance;
			self::$instance = new self();
			return self::$instance;
		}

		public function admin_init() {
			register_setting('MS_WebForm', 'ms_api_url');
			register_setting('MS_WebForm', 'ms_api_token');
			register_setting('MS_WebForm', 'ms_unique_id');
			if(!get_option('ms_api_url', false) || !get_option('ms_api_token', false))  add_action('admin_notices', [&$this, 'admin_notice']);
		}
		public function admin_notice() {
			$plugin = urlencode(explode('plugins/', str_replace('\\', '/', __FILE__))[1]);
			?>
			<div class="notice notice-info">
				<p>You need to setup <a href="options-general.php?page=<?=$plugin?>">MailScout WebForm Integration</a> before you can continue to use.</p>
			</div>
			<?php
		}

		public function admin_menu() {
			add_options_page('MS WebForm', 'MS WebForm', 'manage_options', __FILE__, array(&$this, 'admin_panel'));
		}
		public function admin_panel() {
			if(!is_admin()) return;
			$error = '';
			if(get_option('ms_api_url') && get_option('ms_api_token')) {
				$team = wp_remote_get( get_option( 'ms_api_url' ) . 'teams', [
					'headers' => [
						'Authorization' => 'Bearer ' . get_option( 'ms_api_token' ),
						'Accept'        => 'Application/json'
					]
				] );
				if ( $team->errors ) {
					update_option( 'ms_api_url', '' );
					update_option( 'ms_unique_id', '' );
					$error = 'Invalid API url.';
				} else {
					$code = $team['response']['code'];
					if ( $code != 200 ) {
						if ( $code == 404 ) {
							update_option( 'ms_api_url', '' );
							update_option( 'ms_unique_id', '' );
							$error = 'Invalid API url.';
						} elseif ( $code == 401 ) {
							update_option( 'ms_api_token', '' );
							update_option( 'ms_unique_id', '' );
							$error = 'Invalid API token.';
						}
					} else {
						$team = json_decode( $team['body'], 1 );
						$unique_id = $team['0']['unique_id'];
						update_option( 'ms_unique_id', $unique_id );
					}
				}
			}
			wp_enqueue_style("ms_webform", plugins_url("css/styles.css", __FILE__));
			?>
			<div class="wrap">
				<?php screen_icon(); ?>
				<h2>TX Affiliate Setup</h2>
				<div class="form">
                    <?php if($error!='') {?>
                        <div class="notice notice-info">
                            <p><?php echo $error; ?></p>
                        </div>
                    <?php } ?>
					<form name="dofollow" action="options.php" method="post">
						<?php settings_fields('MS_WebForm'); ?>
						<ul class="form-style-1">
							<li>
								<label>MailScout API URL<span class="required">*</span></label>
								<input type="text" class="field-long" id="ms_api_url" name="ms_api_url" value="<?php echo get_option('ms_api_url'); ?>" placeholder="http://mailscout.io/api/v1/" required/>
							</li>
							<li>
								<label>MailScout API TOKEN</label>
								<input type="text" class="field-long" id="ms_api_token" name="ms_api_token" value="<?php echo get_option('ms_api_token'); ?>" required/>
							</li>
							<li>
								<input type="submit" value="Save" />
							</li>
						</ul>
					</form>
				</div>
			</div>
			<?php
		}

		public function add_media_button() {
			wp_enqueue_style("ms_webform", plugins_url("css/styles.css", __FILE__));
			wp_register_script("ms_webform", plugins_url("js/script.js", __FILE__), array("jquery"));
			wp_enqueue_script("ms_webform");
			if(!get_option('ms_api_url', false) || !get_option('ms_api_token', false)) return;
			?>
            <select id="webforms" class="field-long" onchange="javascript:inject(this)">
                <option value="">Inject WebForm</option>
            </select>
            <?php
		}

		public function load_webforms() {
			if(!is_admin() || parse_url($_SERVER['HTTP_REFERER'])['host']!==$_SERVER['HTTP_HOST']) wp_die(0);
			ob_clean();
			$webforms = wp_remote_get(get_option('ms_api_url').'webforms?limit=500', ['headers'=>['Authorization'=>'Bearer '.get_option('ms_api_token'), 'Accept'=>'Application/json']]);
			print json_encode(array_merge(['team_id'=>get_option('ms_unique_id')], ['webforms'=>json_decode($webforms['body'], 1)['data']]));
			wp_die();
		}

		public function get_web_form($attr) {
		    return '<script type="text/javascript" src="'.get_option('ms_api_url').'webforms/assets/js/'.$attr['id'].'?t='.get_option('ms_unique_id').'"></script>';
        }
	}
	MS_WebForm::start();
}